using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;

    public Camera camaraPrimeraPersona;

    public float magnitudSalto;
    private Rigidbody rb;

    public int saltoMaximo = 2;
    static int saltoContador = 0;

    float tiempoCrecedor = 5;

    public GameManager manager;

    public Text textfield;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();

        GestorDeAudio.instancia.ReproducirSonido("Musica");
    }

    private void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetMouseButtonDown(0))
        {
            GestorDeAudio.instancia.ReproducirSonido("Disparo");

            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 15)
            {
                Debug.Log("El rayo toc� al objeto: " + hit.collider.name);
            }

            if (hit.collider.GetComponent("KillScript") != null)
            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                KillScript scriptMuerte = (KillScript)objetoTocado.GetComponent(typeof(KillScript));

                scriptMuerte.muerte();
            }
        }

        // R para reiniciar.
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }

        // Jump higher, baby!
        if (Input.GetKeyDown(KeyCode.Space) && saltoContador < saltoMaximo)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            saltoContador++;
        }

        // let you down
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            rb.AddForce(Vector3.down * 70, ForceMode.Impulse);
        }

        // Reiniciar contador y quitar efecto.
        if (tiempoCrecedor == 0)
        {
            gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
        }

        // P para pausar (probando el detener la pantalla.)
        if (Input.GetKeyDown(KeyCode.P))
        {
            manager.Pause();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("OutBounds"))
        {
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }

        // Reiniciar el salto de tocar la geometr�a.
        if (collision.gameObject.CompareTag("Geometry"))
        {
            saltoContador = 0;
        }

        // Acelerar si tocamos un Acelerador
        if (collision.gameObject.CompareTag("Aceleradores"))
        {
            textfield.text = "�Acelerando!";
            rapidezDesplazamiento += 5f;
            Destroy(collision.gameObject);
        }

        // Crecer si tocamos un Crecedor
        if (collision.gameObject.CompareTag("Crecedores"))
        {
            gameObject.transform.localScale += new Vector3(5f, 5f, 5f);
            Destroy(collision.gameObject);
            StartCoroutine(ComenzarCronometro(5));
            textfield.text = "�Crecedor obtenido!";
        }

        if (collision.gameObject.CompareTag("Enemy"))
        {
            manager.GameOver();
        }

        if (collision.gameObject.CompareTag("Victory"))
        {
            manager.YouWon();
        }
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 5)
    {
        tiempoCrecedor = valorCronometro;
        while (tiempoCrecedor > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoCrecedor--;
        }
    }
}
