using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject jugador;
    static bool pause = false;
    static bool lost = false;
    public float tiempoGameOver;

    public Text textfield;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CronometroGameOver(60));
    }

    // Update is called once per frame
    void Update()
    {
        if (tiempoGameOver == 0)
        { 
            GameOver();
        }

        if (Input.GetKeyDown(KeyCode.R) && lost)
        {
            Time.timeScale = 1;
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void Pause()
    {
        if (!pause)
        {
            textfield.text = "Juego pausado.";
            Time.timeScale = 0;
            pause = true;
        }
        else if (pause)
        {
            textfield.text = "Juego reanudado.";
            Time.timeScale = 1;
            pause = false;
        }
    }

    public void GameOver()
    {
        Debug.Log("Hey, it's game over.");
        textfield.text = "Has muerto. Pulsa R dos veces para reiniciar.";
        lost = true;
        Time.timeScale = 0;
        pause = true;
    }

    public void YouWon()
    {
        Debug.Log("Hey, you won!");
        textfield.text = "No s� c�mo, pero digamos que has salvado al mundo.";
        lost = true;
        Time.timeScale = 0;
        pause = true;
    }

    public IEnumerator CronometroGameOver(float valorCronometro = 500)
    {
        tiempoGameOver = valorCronometro;
        while (tiempoGameOver > 0)
        {
            Debug.Log("Restan " + tiempoGameOver + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoGameOver--;
        }
    }
}
