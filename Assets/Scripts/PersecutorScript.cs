using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersecutorScript : MonoBehaviour
{
    public GameObject Player;
    public int rapidez = 10;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Player.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
}
