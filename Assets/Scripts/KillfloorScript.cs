using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillfloorScript : MonoBehaviour
{
    static float flag = -1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(1f * flag, 0, 0);

        if (transform.position.x < 20)
        {
            flag = 1f;
        }
        else if (transform.position.x > 140)
        {
            flag = -1f;
        }
    }
}
